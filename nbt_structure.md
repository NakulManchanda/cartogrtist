This is the .schematic NBT structure that we care about. Coordinates in schematics range from (0,0,0) to (Width-1, Height-1, Length-1).

* **Schematic** (Compound): Schematic data.
    * **Width** (Short): Size along the X axis
    * **Height** (Short): Size along the Y axis.
    * **Length** (Short): Size along the Z axis.
    * **Materials** (String): This will be *Classic* for schematics exported from [Minecraft Classic] levels, *Pocket* for those from [Pocket Edition] levels, and *Alpha* for those from [Minecraft Alpha] and newer levels.
    * **Blocks** (Byte Array): [Block IDs] defining the terrain. 8 bits per block. Sorted by height (bottom to top) then length then width -- the index of the block at X,Y,Z is (Y×length + Z)×width + X.
    * **Data** (Byte Array): [Block data] additionally defining parts of the terrain. Only the lower 4 bits of each byte are used. (Unlike in the [chunk format], the block data in the schematic format occupies a full byte per block.)
    * **Icon** (Compound): Used by Schematica as an item to use as an icon for the schematic.  Does not include a Slot tag.
        * **Count** (Byte):  Number of [items] stacked in this inventory slot. Any item can be stacked, including tools, armor, and vehicles. Range is -128 to 127. Values of 1 are not displayed in-game. Values below 1 are displayed in red.
        * **id** (String): [Item/Block ID]. If not specified, Minecraft changes the item to [stone] when loading the chunk or summoning the item.
        * **tag** (Compound): Additional information about the item, discussed more in the subsections of [the item structure section]. This tag is optional for most items.
    * **SchematicaMapping** (Compound): ID mapping for the version this schematic was saved in, used by Schematica.  Only provided for materials used in the schematic.
        * **[name]** (Short): Indicates that name has the given ID (e.g. *[name]* being `minecraft:stone` and the value being `1`).
    * **BlockIDs** (Compound): MCEdit2-only; maps numeric block IDs to textual IDs.
        * **[number]** (String): The textual ID for the given number (e.g. *[number]* is `1` and the value is `minecraft:stone`)

[Minecraft Classic]: https://minecraft.gamepedia.com/Minecraft_Classic
[Pocket Edition]: https://minecraft.gamepedia.com/Pocket_Edition
[Minecraft Alpha]: https://minecraft.gamepedia.com/Minecraft_Alpha
[Block IDs]: https://minecraft.gamepedia.com/Data_values#Block_IDs
[Block data]: https://minecraft.gamepedia.com/Data_values#Data
[chunk format]: https://minecraft.gamepedia.com/Chunk_format
[items]: https://minecraft.gamepedia.com/Item
[Item/Block ID]: https://minecraft.gamepedia.com/Data_values
[stone]: https://minecraft.gamepedia.com/Stone
[the item structure section]: https://minecraft.gamepedia.com/Player.dat_format#Item_structure
