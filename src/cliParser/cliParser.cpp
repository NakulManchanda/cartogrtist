/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "cliParser.h"

#include <string>
#include <stdexcept>
#include <stdint.h>
#include <utility>

#include <tclap/CmdLine.h>
#include <tclap/StdOutput.h>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "palette.h"
#include "cmakeVars.h"

extern template class std::vector<std::string>;
extern template class TCLAP::UnlabeledValueArg<std::string>;
extern template class TCLAP::ValuesConstraint<std::string>;
extern template class TCLAP::ValueArg<std::string>;

//! Overrides TCLAP's version output with a custom output.
class cartogVersionOutput : public TCLAP::StdOutput
{
    public:
    virtual void version(TCLAP::CmdLineInterface& c)
    {
        std::cout << "cartogrtist " << CMakeVars::version << '\n'
                  << "by dan9er (https://d9r.weebly.com) & contributors\n"
                  << "Licenced under GNU AGPLv3 (https://www.gnu.org/licenses/)\n"
                  << "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
                  << "Source: https://gitlab.com/dan9er/cartogrtist" << std::endl;
    }
};

//--------------//
// CTORS & DTOR //
//--------------//

cliParser::cliParser(cliParser&&)
    = default;

cliParser& cliParser::operator=(cliParser&& c)
{
    this->inputPath  = std::move(c.inputPath);
    this->outputPath = std::move(c.outputPath);
    this->pal        = std::move(c.pal);
    this->method     = std::move(c.method);

    return *this;
}

cliParser::cliParser(int argc, char const* argv[])
{
    // arg parser
    TCLAP::CmdLine cmd
    (
        // program description
        "Converts an image into a Minecraft mapart.",
        // delimiter (seperator between args)
        ' '
    );

    // use our subclassed outputer to override the version message
    cartogVersionOutput verOut;
    cmd.setOutput(&verOut);

    // filenames
    TCLAP::UnlabeledValueArg<std::string> inputPathValue
    (
        // name
        "input",
        // description
        "Filename of image to convert. The image must have a size of 128x128. If '-', read from standard input.",
        // required?
        true,
        // default value
        "",
        // type description
        "*.png, etc.|-",
        // cmd.add(inputPathValue)
        cmd
    ),
    outputPathValue
    (
        "output",
        "Filename for outputted schematic file. If '-', write to standard output. If empty, defaults to '[input].schematic'.",
        false,
        "",
        "*.schematic|-",
        cmd
    );

    // palette
    std::vector<std::string> paletteVec =
    {
        "_1.12"
    };
    TCLAP::ValuesConstraint<std::string> paletteVals(paletteVec);
    TCLAP::ValueArg<std::string> paletteValue
    (
        // short flag
        "p",
        // long flag
        "palette",
        // description
        "Color palette to use. Defaults to '_1.12'. See README.md for details.",
        // required?
        false,
        // default value
        "_1.12",
        // constraint
        &paletteVals,
        // cmd.add(paletteValue)
        cmd
    );

    // method
    std::vector<std::string> methodVec =
    {
        "flat",
        "staircase"
    };
    TCLAP::ValuesConstraint<std::string> methodVals(methodVec);
    TCLAP::ValueArg<std::string> methodValue
    (
        // short flag
        "m",
        // long flag
        "method",
        // description
        "In-game map-making method. Defaults to 'staircase'. See README.md for details.",
        // required?
        false,
        // default value
        "staircase",
        // constraint
        &methodVals,
        // cmd.add(methodValue)
        cmd
    );

    // logging level
    TCLAP::MultiSwitchArg verboseSwitch
    (
        "v",
        "verbose",
        "Increase the logging level. See README.md for details.",
        cmd
    ),
    quietSwitch
    (
        "q",
        "quiet",
        "Decrease the logging level. See README.md for details.",
        cmd
    );

    // colorless logging
    TCLAP::SwitchArg colorlessLogSwitch
    (
        "C",
        "colorless-log",
        "Use tags instead of color in logging. Useful if redirecting logger output to file.",
        cmd
    );

    // parse flags
    cmd.parse(argc, argv);

    // grab the values

    if (!colorlessLogSwitch.getValue())
    {
        // set default logger
        spdlog::set_default_logger
        (
            std::make_shared<spdlog::logger>
            (
                "colorLogger",
                std::make_shared<spdlog::sinks::stderr_color_sink_mt>()
            )
        );

        // set default logger pattern
        spdlog::set_pattern("%^%v%$");
    }

    int verbInt = quietSwitch.getValue() - verboseSwitch.getValue() + 2;
    if (verbInt < 1)
        spdlog::set_level(spdlog::level::trace);
    else if (verbInt > 5)
        spdlog::set_level(spdlog::level::off);
    else
        spdlog::set_level(static_cast<spdlog::level::level_enum>(std::move(verbInt)));

    this->inputPath = inputPathValue.getValue();
    this->outputPath = outputPathValue.getValue();
    if (this->outputPath.empty())
    {
        if (this->inputPath == "-")
            this->outputPath = "-";
        else
            this->outputPath = this->inputPath + ".schematic";
    }

    std::string i = paletteValue.getValue();
    if (i == "_1.12")
        {this->pal = palette(palette::builtin::one_twelve);}
    else
        {throw std::runtime_error("paletteValue returned unexpected string");}

    i = methodValue.getValue();
    if (i == "flat")
        {this->method = palette::methodType::flat;}
    else if (i == "staircase")
        {this->method = palette::methodType::staircase;}
    else
        {throw std::runtime_error("methodValue returned unexpected string");}
}

/*virtual*/ cliParser::~cliParser()
    = default;

//----------------//
// PUBLIC MEMBERS //
//----------------//

const std::string& cliParser::getInputPath() const
    {return this->inputPath;}

const std::string& cliParser::getOutputPath() const
    {return this->outputPath;}

const palette& cliParser::getPalette() const
    {return this->pal;}

const palette::methodType& cliParser::getMethod() const
    {return this->method;}
