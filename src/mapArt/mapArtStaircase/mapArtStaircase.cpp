/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "mapArtStaircase.h"

#include <array>
#include <memory>
#include <utility>

#include <Magick++.h>

#include <spdlog/spdlog.h>

#include "staircaseColumn.h"
#include "palette.h"
#include "schematic.h"

extern template class schematic<128,130,129>;

//--------------//
// CTORS & DTOR //
//--------------//

mapArtStaircase::mapArtStaircase() :
    columns(new mapArtStaircase::array_type())
{
    spdlog::trace("mapArtStaircase::mapArtStaircase()");}

mapArtStaircase::mapArtStaircase(const mapArtStaircase& m) :
    columns(new mapArtStaircase::array_type(*(m.columns)))
{
    spdlog::trace("mapArtStaircase::mapArtStaircase(const mapArtStaircase&)");}

mapArtStaircase& mapArtStaircase::operator=(const mapArtStaircase& m)
{
    spdlog::trace("mapArtStaircase::operator=(const mapArtStaircase&)");

    // don't do anything if trying to copy to self
    if (this->columns != m.columns)
        this->columns.reset(new mapArtStaircase::array_type(*(m.columns)));

    return *this;
}

mapArtStaircase::mapArtStaircase(mapArtStaircase&& m) :
    columns(m.columns.release())
{
    spdlog::trace("mapArtStaircase::mapArtStaircase(mapArtStaircase&&)");}

mapArtStaircase& mapArtStaircase::operator=(mapArtStaircase&& m)
{
    spdlog::trace("mapArtStaircase::operator=(mapArtStaircase&&)");

    // don't do anything if trying to move to self
    if (this->columns != m.columns)
        this->columns.reset(m.columns.release());

    return *this;
}

mapArtStaircase::mapArtStaircase(mapArtStaircase::array_type&& a) :
    columns(new mapArtStaircase::array_type(std::move(a)))
{
    spdlog::trace("mapArtStaircase::operator=(mapArtStaircase::array_type&&)");}

mapArtStaircase::mapArtStaircase(const Magick::Image& i, const palette& p) :
    columns(new mapArtStaircase::array_type())
{
    spdlog::trace("mapArtStaircase::mapArtStaircase(Magick::Image&, palette&)");

    // construct staircaseColums with those strips
    for (mapArtStaircase::array_type::size_type x = 0; x != 128; ++x)
    {
        spdlog::debug("x: {}", x);

        Magick::Image j(i);
        j.crop(Magick::Geometry(1,128,x,0));
        this->columns->at(x) = staircaseColumn(std::move(j), p);
    }
}

/*virtual*/ mapArtStaircase::~mapArtStaircase()
    {spdlog::trace("mapArtStaircase::~mapArtStaircase()");}

//----------------//
// PUBLIC MEMBERS //
//----------------//

schematic<128,130,129> mapArtStaircase::exportSchematic()
{
    spdlog::trace("mapArtStaircase::exportSchematic()");

    // create empty schematic
    spdlog::info("-- Initializing empty schematic");
    schematic<128,130,129> ret;

    // place the blocks
    spdlog::info("-- Placing blocks from mapart object");
    for (uint_fast8_t x = 0; x != 128; ++x)
    {
        const staircaseColumn& c = this->columns->at(x);
        for (uint_fast8_t y = 0; y != 129; ++y)
        {
            spdlog::trace("x,y = ({},{})", x, y);
            std::pair<const block&, const int&> p = c.at(y);

            if (p.first.needsSupport())
                ret.at(x, p.second, 128-y) = block(3,0, "minecraft:dirt", Magick::ColorRGB("#000000"));

            ret.at(x, p.second+1, 128-y) = std::move(p.first);
        }
    }

    // return schematic
    return ret;
}
