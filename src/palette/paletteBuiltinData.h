/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <set>

#include <Magick++.h>

#include "block.h"

extern template class std::set<block>;

//! A collection of builtin standard \ref palette s.
/*! \see palette::builtin
    \todo Make these \c constexpr */
namespace paletteBuiltinData
{
    //! \see palette::builtin::one_twelve
    const std::set<block> one_twelve
    ({
        block
        (
            2, 0,
            "minecraft:grass",
        	Magick::ColorRGB("#597d27"),
        	Magick::ColorRGB("#6d9930"),
        	Magick::ColorRGB("#7fb238")
        ),
        block
        (
            5, 2,
            "minecraft:planks",
        	Magick::ColorRGB("#aea473"),
        	Magick::ColorRGB("#d5c98c"),
        	Magick::ColorRGB("#f7e9a3")
        ),
        block
        (
            30, 0,
            "minecraft:web",
        	Magick::ColorRGB("#8c8c8c"),
        	Magick::ColorRGB("#ababab"),
        	Magick::ColorRGB("#c7c7c7")
        ),
        block
        (
            46, 0,
            "minecraft:tnt",
        	Magick::ColorRGB("#b40000"),
        	Magick::ColorRGB("#dc0000"),
        	Magick::ColorRGB("#ff0000")
        ),
        block
        (
            79, 0,
            "minecraft:ice",
        	Magick::ColorRGB("#7070b4"),
        	Magick::ColorRGB("#8a8adc"),
        	Magick::ColorRGB("#a0a0ff")
        ),
        block
        (
            101, 0,
            "minecraft:iron_bars",
        	Magick::ColorRGB("#757575"),
        	Magick::ColorRGB("#909090"),
        	Magick::ColorRGB("#a7a7a7"),
            true
        ),
        block
        (
            37, 0,
            "minecraft:yellow_flower",
        	Magick::ColorRGB("#005700"),
        	Magick::ColorRGB("#006a00"),
        	Magick::ColorRGB("#007c00"),
            true
        ),
        block
        (
            35, 0,
            "minecraft:wool",
        	Magick::ColorRGB("#b4b4b4"),
        	Magick::ColorRGB("#dcdcdc"),
        	Magick::ColorRGB("#ffffff")
        ),
        block
        (
            82, 0,
            "minecraft:clay",
        	Magick::ColorRGB("#737681"),
        	Magick::ColorRGB("#8d909e"),
        	Magick::ColorRGB("#a4a8b8")
        ),
        block
        (
            5, 3,
            "minecraft:planks",
        	Magick::ColorRGB("#6a4c36"),
        	Magick::ColorRGB("#825e42"),
        	Magick::ColorRGB("#976d4d")
        ),
        block
        (
            4, 0,
            "minecraft:cobblestone",
        	Magick::ColorRGB("#4f4f4f"),
        	Magick::ColorRGB("#606060"),
        	Magick::ColorRGB("#707070")
        ),
        block
        (
            5, 0,
            "minecraft:planks",
        	Magick::ColorRGB("#645432"),
        	Magick::ColorRGB("#7b663e"),
        	Magick::ColorRGB("#8f7748")
        ),
        block
        (
            1, 3,
            "minecraft:stone",
        	Magick::ColorRGB("#b4b1ac"),
        	Magick::ColorRGB("#dcd9d3"),
        	Magick::ColorRGB("#fffcf5")
        ),
        block
        (
            5, 4,
            "minecraft:planks",
        	Magick::ColorRGB("#985924"),
        	Magick::ColorRGB("#ba6d2c"),
        	Magick::ColorRGB("#d87f33")
        ),
        block
        (
            35, 2,
            "minecraft:wool",
        	Magick::ColorRGB("#7d3598"),
        	Magick::ColorRGB("#9941ba"),
        	Magick::ColorRGB("#b24cd8")
        ),
        block
        (
            35, 3,
            "minecraft:wool",
        	Magick::ColorRGB("#486c98"),
        	Magick::ColorRGB("#5884ba"),
        	Magick::ColorRGB("#6699d8")
        ),
        block
        (
            35, 4,
            "minecraft:wool",
        	Magick::ColorRGB("#a1a124"),
        	Magick::ColorRGB("#c5c52c"),
        	Magick::ColorRGB("#e5e533")
        ),
        block
        (
            35, 5,
            "minecraft:wool",
        	Magick::ColorRGB("#599011"),
        	Magick::ColorRGB("#6db015"),
        	Magick::ColorRGB("#7fcc19")
        ),
        block
        (
            35, 6,
            "minecraft:wool",
        	Magick::ColorRGB("#aa5974"),
        	Magick::ColorRGB("#d06d8e"),
        	Magick::ColorRGB("#f27fa5")
        ),
        block
        (
            35, 7,
            "minecraft:wool",
        	Magick::ColorRGB("#353535"),
        	Magick::ColorRGB("#414141"),
        	Magick::ColorRGB("#4c4c4c")
        ),
        block
        (
            35, 8,
            "minecraft:wool",
        	Magick::ColorRGB("#6c6c6c"),
        	Magick::ColorRGB("#848484"),
        	Magick::ColorRGB("#999999")
        ),
        block
        (
            35, 9,
            "minecraft:wool",
        	Magick::ColorRGB("#35596c"),
        	Magick::ColorRGB("#416d84"),
        	Magick::ColorRGB("#4c7f99")
        ),
        block
        (
            35, 10,
            "minecraft:wool",
        	Magick::ColorRGB("#592c7d"),
        	Magick::ColorRGB("#6d3699"),
        	Magick::ColorRGB("#7f3fb2")
        ),
        block
        (
            35, 11,
            "minecraft:wool",
        	Magick::ColorRGB("#24357d"),
        	Magick::ColorRGB("#2c4199"),
        	Magick::ColorRGB("#334cb2")
        ),
        block
        (
            5, 5,
            "minecraft:planks",
        	Magick::ColorRGB("#483524"),
        	Magick::ColorRGB("#58412c"),
        	Magick::ColorRGB("#664c33")
        ),
        block
        (
            35, 13,
            "minecraft:wool",
        	Magick::ColorRGB("#485924"),
        	Magick::ColorRGB("#586d2c"),
        	Magick::ColorRGB("#667f33")
        ),
        block
        (
            35, 14,
            "minecraft:wool",
        	Magick::ColorRGB("#6c2424"),
        	Magick::ColorRGB("#842c2c"),
        	Magick::ColorRGB("#993333")
        ),
        block
        (
            35, 15,
            "minecraft:wool",
        	Magick::ColorRGB("#111111"),
        	Magick::ColorRGB("#151515"),
        	Magick::ColorRGB("#191919")
        ),
        block
        (
            147, 0,
            "minecraft:light_weighted_pressure_plate",
        	Magick::ColorRGB("#b0a836"),
        	Magick::ColorRGB("#d7cd42"),
        	Magick::ColorRGB("#faee4d"),
            true
        ),
        block
        (
            57, 0,
            "minecraft:diamond_block",
        	Magick::ColorRGB("#409a96"),
        	Magick::ColorRGB("#4fbcb7"),
        	Magick::ColorRGB("#5cdbd5")
        ),
        block
        (
            22, 0,
            "minecraft:lapis_block",
        	Magick::ColorRGB("#345ab4"),
        	Magick::ColorRGB("#3f6edc"),
        	Magick::ColorRGB("#4a80ff")
        ),
        block
        (
            133, 0,
            "minecraft:emerald_block",
        	Magick::ColorRGB("#009928"),
        	Magick::ColorRGB("#00bb32"),
        	Magick::ColorRGB("#00d93a")
        ),
        block
        (
            5, 1,
            "minecraft:planks",
        	Magick::ColorRGB("#5b3c22"),
        	Magick::ColorRGB("#6f4a2a"),
        	Magick::ColorRGB("#815631")
        ),
        block
        (
            87, 0,
            "minecraft:netherrack",
        	Magick::ColorRGB("#4f0100"),
        	Magick::ColorRGB("#600100"),
        	Magick::ColorRGB("#700200")
        ),
        block
        (
            172, 0,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#937c71"),
        	Magick::ColorRGB("#b4988a"),
        	Magick::ColorRGB("#d1b1a1")
        ),
        block
        (
            172, 1,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#703919"),
        	Magick::ColorRGB("#89461f"),
        	Magick::ColorRGB("#9f5224")
        ),
        block
        (
            172, 2,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#693d4c"),
        	Magick::ColorRGB("#804b5d"),
        	Magick::ColorRGB("#95576c")
        ),
        block
        (
            172, 3,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#4f4c61"),
        	Magick::ColorRGB("#605d77"),
        	Magick::ColorRGB("#706c8a")
        ),
        block
        (
            172, 4,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#835d19"),
        	Magick::ColorRGB("#a0721f"),
        	Magick::ColorRGB("#ba8524")
        ),
        block
        (
            172, 5,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#485225"),
        	Magick::ColorRGB("#58642d"),
        	Magick::ColorRGB("#677535")
        ),
        block
        (
            172, 6,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#703637"),
        	Magick::ColorRGB("#8a4243"),
        	Magick::ColorRGB("#a04d4e")
        ),
        block
        (
            172, 7,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#281c18"),
        	Magick::ColorRGB("#31231e"),
        	Magick::ColorRGB("#392923")
        ),
        block
        (
            172, 8,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#5f4b45"),
        	Magick::ColorRGB("#745c54"),
        	Magick::ColorRGB("#876b62")
        ),
        block
        (
            172, 9,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#3d4040"),
        	Magick::ColorRGB("#4b4f4f"),
        	Magick::ColorRGB("#575c5c")
        ),
        block
        (
            172, 10,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#56333e"),
        	Magick::ColorRGB("#693e4b"),
        	Magick::ColorRGB("#7a4958")
        ),
        block
        (
            172, 11,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#352b40"),
        	Magick::ColorRGB("#41354f"),
        	Magick::ColorRGB("#4c3e5c")
        ),
        block
        (
            172, 12,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#352318"),
        	Magick::ColorRGB("#412b1e"),
        	Magick::ColorRGB("#4c3223")
        ),
        block
        (
            172, 13,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#35391d"),
        	Magick::ColorRGB("#414624"),
        	Magick::ColorRGB("#4c522a")
        ),
        block
        (
            172, 14,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#642a20"),
        	Magick::ColorRGB("#7a3327"),
        	Magick::ColorRGB("#8e3c2e")
        ),
        block
        (
            172, 15,
            "minecraft:stained_hardened_clay",
        	Magick::ColorRGB("#1a0f0b"),
        	Magick::ColorRGB("#1f120d"),
        	Magick::ColorRGB("#251610")
        )
    });
}
