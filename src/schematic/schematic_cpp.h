/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <string>
#include <array>
#include <memory>
#include <utility>

#include <libnbtplusplus/nbt_tags.h>
#include <libnbtplusplus/io/stream_writer.h>

#include <spdlog/spdlog.h>

#include "block.h"

/*! \file
    \brief Internal include for the schematic template class. <em>DO NOT INCLUDE DIRECTLY.</em> */

//--------------//
// CTORS & DTOR //
//--------------//

template <std::size_t W, std::size_t H, std::size_t L>
schematic<W,H,L>::schematic() :
    blocks(new schematic<W,H,L>::array_type())
{
    spdlog::trace("schematic<{},{},{}>::schematic()", W,H,L);}

template <std::size_t W, std::size_t H, std::size_t L>
schematic<W,H,L>::schematic(const schematic<W,H,L>& s) :
    blocks(new schematic<W,H,L>::array_type(*(s.blocks)))
{
    spdlog::trace("schematic<{0},{1},{2}>::schematic(const schematic<{0},{1},{2}>&)", W,H,L);}

template <std::size_t W, std::size_t H, std::size_t L>
schematic<W,H,L>& schematic<W,H,L>::operator=(const schematic<W,H,L>& s)
{
    spdlog::trace("schematic<{0},{1},{2}>::operator=(const schematic<{0},{1},{2}>&)", W,H,L);

    // don't do anything if trying to copy to self
    if (this->blocks != s.blocks)
        this->blocks.reset(new schematic<W,H,L>::array_type(*(s.blocks)));

    return *this;
}

template <std::size_t W, std::size_t H, std::size_t L>
schematic<W,H,L>::schematic(schematic<W,H,L>&& s) :
    blocks(s.blocks.release())
{
    spdlog::trace("schematic<{0},{1},{2}>::schematic(schematic<{0},{1},{2}>&&)", W,H,L);}

template <std::size_t W, std::size_t H, std::size_t L>
schematic<W,H,L>& schematic<W,H,L>::operator=(schematic<W,H,L>&& s)
{
    spdlog::trace("schematic<{0},{1},{2}>::operator=(schematic<{0},{1},{2}>&&)", W,H,L);

    // don't do anything if trying to move to self
    if (this->blocks != s.blocks)
        this->blocks.reset(s.blocks.release());

    return *this;
}

template <std::size_t W, std::size_t H, std::size_t L>
schematic<W,H,L>::schematic(schematic<W,H,L>::array_type&& a) :
    blocks(new schematic<W,H,L>::array_type(std::move(a)))
{
    spdlog::trace("schematic<{0},{1},{2}>::schematic(array_type&&)", W,H,L);}

template <std::size_t W, std::size_t H, std::size_t L>
/*virtual*/ schematic<W,H,L>::~schematic()
    {spdlog::trace("schematic<{},{},{}>::~schematic()", W,H,L);}

//----------------//
// PUBLIC MEMBERS //
//----------------//

template <std::size_t W, std::size_t H, std::size_t L>
block& schematic<W,H,L>::at
(
    const schematic<W,H,L>::size_type& x,
    const schematic<W,H,L>::size_type& y,
    const schematic<W,H,L>::size_type& z
)
{
    spdlog::trace("schematic<{},{},{}>::at(const size_type& x = {}, y = {}, z = {})", W,H,L, x,y,z);
    return this->blocks->at(this->flattenIndex(x,y,z));
}
template <std::size_t W, std::size_t H, std::size_t L>
const block& schematic<W,H,L>::at
(
    const schematic<W,H,L>::size_type& x,
    const schematic<W,H,L>::size_type& y,
    const schematic<W,H,L>::size_type& z
) const
{
    spdlog::trace("schematic<{},{},{}>::at(const size_type& x = {}, y = {}, z = {}) const", W,H,L, x,y,z);
    return this->blocks->at(this->flattenIndex(x,y,z));
}

template <std::size_t W, std::size_t H, std::size_t L>
schematic<W,H,L>& schematic<W,H,L>::fill
(
    const schematic<W,H,L>::size_type& x1,
    const schematic<W,H,L>::size_type& y1,
    const schematic<W,H,L>::size_type& z1,
    const schematic<W,H,L>::size_type& x2,
    const schematic<W,H,L>::size_type& y2,
    const schematic<W,H,L>::size_type& z2,
    const block& b
)
{
    spdlog::trace("schematic<{},{},{}>::fill(const size_type& x1 = {}, y1 = {}, z1 = {}, x2 = {}, y2 = {}, z2 = {}, const block& b = {})",
        W,H,L, x1,y1,z1, x2,y2,z2, b);

    for (typename schematic<W,H,L>::size_type x = x1; x <= x2; ++x)
        for (typename schematic<W,H,L>::size_type y = y1; y <= y2; ++y)
            for (typename schematic<W,H,L>::size_type z = z1; z <= z2; ++z)
                this->at(x,y,z) = b;

    return *this;
}

template <std::size_t W, std::size_t H, std::size_t L>
inline typename schematic<W,H,L>::const_iterator schematic<W,H,L>::cbegin() const
    {return this->blocks->cbegin();}

template <std::size_t W, std::size_t H, std::size_t L>
inline typename schematic<W,H,L>::const_iterator schematic<W,H,L>::cend() const
    {return this->blocks->cend();}

//-----------------//
// PRIVATE MEMBERS //
//-----------------//

template <std::size_t W, std::size_t H, std::size_t L>
inline const typename schematic<W,H,L>::size_type schematic<W,H,L>::flattenIndex
(
    const schematic<W,H,L>::size_type& x,
    const schematic<W,H,L>::size_type& y,
    const schematic<W,H,L>::size_type& z
) const
{
    return (y * L + z) * W + x;}

//-------------//
// NON-MEMBERS //
//-------------//

template <std::size_t W, std::size_t H, std::size_t L>
std::ostream& operator<<(std::ostream& lhs, const schematic<W,H,L>& rhs)
{
    spdlog::trace("operator<<(std::ostream&, const schematic<{},{},{}>&)", W,H,L);

    // create block id vectors
    std::unique_ptr<std::array<int8_t, (W*H*L)>> bVec(new std::array<int8_t, (W*H*L)>()),
                                                 dVec(new std::array<int8_t, (W*H*L)>());

    // create number-to-namespace id compound tags
    nbt::tag_compound schematicaComp,
                      mceditComp    ;

    // go through lhs.blocks
    spdlog::info("-- Extracting block & data ids");
    typename std::array<int8_t, (W*H*L)>::iterator bIter = bVec->begin(),
                                                   dIter = dVec->begin();
    for (typename schematic<W,H,L>::const_iterator iter = rhs.cbegin(); iter != rhs.cend(); ++iter)
    {
        // get block from rhs iterator
        const block& b = *iter;

        // set the block ids inside bVec and dVec
        *bIter = b.blockId();
        *dIter = b.dataId();

        // add the namespace-number id correlation to schemComp & mceditComp
        schematicaComp.put(b.namespacedId(), nbt::tag_short(b.blockId()));
        mceditComp.put(std::to_string(b.blockId()), nbt::tag_string(b.namespacedId()));

        // advance the array iterators
        ++bIter;
        ++dIter;
    }
    // bIter & dIter now == end() of their respective arrays

    // use nbt++ to write to lhs
    spdlog::info("-- Writing to file using nbt++");
    nbt::io::stream_writer(lhs).write_tag
    (
        "Schematic", nbt::tag_compound
        ({
            {"Width",        nbt::tag_short     (W)},
            {"Height",       nbt::tag_short     (H)},
            {"Length",       nbt::tag_short     (L)},
            {"Materials",    nbt::tag_string    ("Alpha")},
            {"Blocks",       nbt::tag_byte_array(std::vector<int8_t>(bVec->begin(), bIter))},
            {"Data",         nbt::tag_byte_array(std::vector<int8_t>(dVec->begin(), dIter))},
            {"Entities",     nbt::tag_list()},
            {"TileEntities", nbt::tag_list()},
            {"Icon",         nbt::tag_compound
            ({
                {"Count", nbt::tag_byte(1)},
                {"id",    nbt::tag_string("minecraft:painting")}
            })},
            {"SchematicaMapping", std::move(schematicaComp)},
            {"BlockIDs",          std::move(mceditComp)}
        })
    );

    // return stream
    return lhs;
}
