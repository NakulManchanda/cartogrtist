/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <array>
#include <memory>

#include <Magick++.h>

#include "palette.h"

extern template class std::array<block,129>;
extern template class std::array<  int,129>;

//! Represents a column in a mapArtStaircase.
class staircaseColumn
{
    public:

        //--------------//
        // CTORS & DTOR //
        //--------------//

        //! Default constructor.
        /*! Initializes \ref blocks to be filled with default \ref block s and \ref height to be filled with zeroes.
            \see block::block() */
        staircaseColumn();

        //! Copy constructor.
        /*! \param c staircaseColumn to copy from. */
        staircaseColumn(const staircaseColumn&);

        //! Copy operator.
        /*! \param c staircaseColumn to copy from.
            \returns A reference to the staircaseColumn copied to. */
        staircaseColumn& operator=(const staircaseColumn&);

        //! Move constructor.
        /*! \param c staircaseColumn to move from. */
        staircaseColumn(staircaseColumn&&);

        //! Move operator.
        /*! \param c staircaseColumn to move from.
            \returns A reference to the staircaseColumn moved to. */
        staircaseColumn& operator=(staircaseColumn&&);

        //! Constructor accepting an Magick::Image and \ref palette.
        /*! \param i The Magick::Image to convert to staircaseColumn.
            \param p The \ref palette to use.
            \warning This constructor assumes \p i conforms to the palette \p p, and so does \e not catch any exceptions thrown by \ref block or \ref palette. */
        staircaseColumn(const Magick::Image&, const palette&);

        //! Destructor.
        virtual ~staircaseColumn();

        //----------------//
        // PUBLIC MEMBERS //
        //----------------//

        //! Get the \ref block and height of a specific position in the staircaseColumn.
        /*! \param n The position in \ref blocks and \ref height to get from.
            \returns A std::pair where:

            - \c .first is a reference to the \ref block in the position \p n .
            - \c .second is a reference to \c .first 's height. */
        std::pair<      block&,       int&> at(std::size_t)      ;

        //! \c const version of at().
        std::pair<const block&, const int&> at(std::size_t) const;

        //! Adds a number to the values in \p height so the lower bound is 0.
        /*! \returns A reference to the staircaseColumn called. */
        staircaseColumn& normalizeHeight();

    private:

        //------//
        // DATA //
        //------//

        //! Contains the \ref block s in the staircaseColumn.
        std::unique_ptr<std::array<block,129>> blocks;

        //! An array of intergers specifing the heights of the staircaseColumn's \ref blocks.
        std::unique_ptr<std::array<  int,129>> height;
};
